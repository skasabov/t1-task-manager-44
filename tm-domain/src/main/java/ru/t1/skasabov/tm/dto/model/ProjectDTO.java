package ru.t1.skasabov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractUserOwnedModelDTO {

    @NotNull
    @Column(nullable = false)
    private String name;

    @Column
    @Nullable
    private String description = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    @Nullable
    @Column(name = "begin_date")
    private Date dateBegin;

    @Nullable
    @Column(name = "end_date")
    private Date dateEnd;

    public ProjectDTO(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        @NotNull final ProjectDTO project = (ProjectDTO) o;
        return name.equals(project.getName());
    }

    @Override
    public int hashCode() {
        return 31 * name.hashCode();
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
