package ru.t1.skasabov.tm.taskmanager.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.repository.dto.SessionDTORepository;
import ru.t1.skasabov.tm.repository.dto.UserDTORepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SessionDTORepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private ISessionDTORepository sessionRepository;

    @NotNull
    protected EntityManager entityManager;

    @Before
    @SneakyThrows
    public void initRepository() {
        entityManager = connectionService.getEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        sessionRepository = new SessionDTORepository(entityManager);
        entityManager.getTransaction().begin();
        sessionRepository.removeAll();
        @NotNull final UserDTO userOne = new UserDTO();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        @NotNull final UserDTO userTwo = new UserDTO();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            if (i <= 5) session.setUserId(USER_ID_ONE);
            else session.setUserId(USER_ID_TWO);
            sessionRepository.add(session);
        }
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(USER_ID_TWO);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = sessionRepository.getSize() + 4;
        @NotNull final List<SessionDTO> actualSessions = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            session.setUserId(USER_ID_ONE);
            actualSessions.add(session);
        }
        sessionRepository.addAll(actualSessions);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<SessionDTO> actualSessions = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            session.setUserId(USER_ID_TWO);
            actualSessions.add(session);
        }
        sessionRepository.set(actualSessions);
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testClearAll() {
        sessionRepository.removeAll();
        Assert.assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        sessionRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<SessionDTO> sessionList = sessionRepository.findAll(USER_ID_TWO);
        sessionRepository.removeAll(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<SessionDTO> sessionList = sessionRepository.findAll();
        Assert.assertEquals(sessionList.size(), sessionRepository.getSize());
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<SessionDTO> sessionList = sessionRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, sessionList.size());
    }

    @Test
    public void testFindById() {
        @NotNull final SessionDTO session = sessionRepository.findAll().get(0);
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        @Nullable final SessionDTO actualSession = sessionRepository.findOneById(sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final SessionDTO session = sessionRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String sessionId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final SessionDTO actualSession = sessionRepository.findOneById(USER_ID_ONE, sessionId);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIdSessionNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(id));
    }

    @Test
    public void testFindByIdSessionNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(sessionRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final SessionDTO session = sessionRepository.findAll().get(0);
        @Nullable final SessionDTO actualSession = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIndexSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndex(0));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final SessionDTO session = sessionRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final SessionDTO actualSession = sessionRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getDate(), actualSession.getDate());
        Assert.assertEquals(session.getRole(), actualSession.getRole());
        Assert.assertEquals(session.getUserId(), actualSession.getUserId());
    }

    @Test
    public void testFindByIndexForUserSessionNotFound() {
        sessionRepository.removeAll();
        Assert.assertNull(sessionRepository.findOneByIndex(USER_ID_ONE, 0));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(USER_ID_ONE);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(USER_ID_TWO);
        sessionRepository.add(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, sessionRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = sessionRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(invalidId));
        Assert.assertTrue(sessionRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(sessionRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(sessionRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = sessionRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final SessionDTO session = sessionRepository.findAll(USER_ID_ONE).get(0);
        sessionRepository.removeOne(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - 1;
        @NotNull final String sessionId = sessionRepository.findAll().get(0).getId();
        sessionRepository.removeOneById(sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = sessionRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String sessionId = sessionRepository.findAll(USER_ID_ONE).get(0).getId();
        sessionRepository.removeOneById(USER_ID_ONE, sessionId);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = sessionRepository.getSize() - 1;
        sessionRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = sessionRepository.getSize(USER_ID_TWO) - 1;
        sessionRepository.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_ID_TWO));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

}
