package ru.t1.skasabov.tm.taskmanager.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.repository.dto.ProjectDTORepository;
import ru.t1.skasabov.tm.repository.dto.TaskDTORepository;
import ru.t1.skasabov.tm.repository.dto.UserDTORepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectDTORepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private IProjectDTORepository projectRepository;

    @NotNull
    protected EntityManager entityManager;

    @Before
    @SneakyThrows
    public void initRepository() {
        entityManager = connectionService.getEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        projectRepository = new ProjectDTORepository(entityManager);
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        @NotNull final List<ProjectDTO> projects = projectRepository.findAll();
        entityManager.getTransaction().begin();
        for (@NotNull final ProjectDTO project : projects) {
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(project.getId());
            taskRepository.removeAll(tasks);
        }
        projectRepository.removeAll();
        @NotNull final UserDTO userOne = new UserDTO();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        @NotNull final UserDTO userTwo = new UserDTO();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectRepository.add(project);
        }
    }

    @Test
    public void testUpdate() {
        @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_TWO).get(0);
        project.setName("Test Project One");
        project.setDescription("Test Description One");
        projectRepository.update(project);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(USER_ID_TWO, project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(USER_ID_TWO, actualProject.getUserId());
        Assert.assertEquals("Test Project One", actualProject.getName());
        Assert.assertEquals("Test Description One", actualProject.getDescription());
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = projectRepository.getSize() + 4;
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_ONE);
            actualProjects.add(project);
        }
        projectRepository.addAll(actualProjects);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_TWO);
            actualProjects.add(project);
        }
        projectRepository.set(actualProjects);
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testClearAll() {
        projectRepository.removeAll();
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = projectRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        projectRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = projectRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAll(USER_ID_TWO);
        projectRepository.removeAll(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAll();
        Assert.assertEquals(projectList.size(), projectRepository.getSize());
    }

    @Test
    public void testFindAllWithNameComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByName();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        actualProjects.add(1, actualProjects.get(NUMBER_OF_ENTRIES - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreated();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByStatus();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            if (i < 4) project.setStatus(Status.COMPLETED);
            else if (i < 7) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test
    public void testFindAllWithNameComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + (i + 5));
            actualProjects.add(project);
        }
        actualProjects.add(0, actualProjects.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByStatusForUser(USER_ID_TWO);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + (i + 5));
            if (i < 3) project.setStatus(Status.COMPLETED);
            else if (i < 5) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindById() {
        @NotNull final ProjectDTO project = projectRepository.findAll().get(0);
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String projectId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(id));
    }

    @Test
    public void testFindByIdProjectNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final ProjectDTO project = projectRepository.findAll().get(0);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIndex(0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.findOneByIndex(1));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexForUserProjectNotFound() {
        projectRepository.removeAll();
        Assert.assertNull(projectRepository.findOneByIndex(USER_ID_ONE, 1));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_ONE);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(invalidId));
        Assert.assertTrue(projectRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = projectRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_ONE).get(0);
        projectRepository.removeOne(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = projectRepository.getSize() - 1;
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        projectRepository.removeOneById(projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = projectRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        projectRepository.removeOneById(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = projectRepository.getSize() - 1;
        projectRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = projectRepository.getSize(USER_ID_TWO) - 1;
        projectRepository.removeOneByIndex(USER_ID_TWO, 1);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_TWO));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

}
