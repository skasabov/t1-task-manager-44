package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.skasabov.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        return entityManager.createQuery("SELECT s FROM SessionDTO s", SessionDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.id = :id", SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s", SessionDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(s) FROM SessionDTO s", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM SessionDTO s WHERE s.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeAll() {
        entityManager.createQuery("DELETE FROM SessionDTO s")
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId AND s.id = :id",
                        SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT s FROM SessionDTO s WHERE s.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(s) FROM SessionDTO s WHERE s.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM SessionDTO s WHERE s.userId = :userId AND s.id = :id")
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM SessionDTO s WHERE s.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }
    
}
