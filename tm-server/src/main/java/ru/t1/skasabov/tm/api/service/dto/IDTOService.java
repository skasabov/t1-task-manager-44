package ru.t1.skasabov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;

import java.util.Collection;
import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    List<M> findAll();

    @NotNull
    Collection<M> addAll(@Nullable Collection<M> models);

    @NotNull
    Collection<M> set(@Nullable Collection<M> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    long getSize();

    @Nullable
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<M> collection);

    void removeAll();

}
