package ru.t1.skasabov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.dto.IUserDTOService;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.exception.user.ExistsEmailException;
import ru.t1.skasabov.tm.exception.user.ExistsLoginException;
import ru.t1.skasabov.tm.exception.user.PasswordHashEmptyException;
import ru.t1.skasabov.tm.exception.user.RoleEmptyException;
import ru.t1.skasabov.tm.repository.dto.ProjectDTORepository;
import ru.t1.skasabov.tm.repository.dto.SessionDTORepository;
import ru.t1.skasabov.tm.repository.dto.TaskDTORepository;
import ru.t1.skasabov.tm.repository.dto.UserDTORepository;
import ru.t1.skasabov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class UserDTOService extends AbstractDTOService<UserDTO> implements IUserDTOService {

    @NotNull
    private final IPropertyService propertyService;

    public UserDTOService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<UserDTO> collection) {
        if (collection == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : collection) {
                @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(user.getId());
                @NotNull final List<ProjectDTO> projects = projectRepository.findAll(user.getId());
                @NotNull final List<TaskDTO> tasks = taskRepository.findAll(user.getId());
                taskRepository.removeAll(tasks);
                sessionRepository.removeAll(sessions);
                for (@NotNull final ProjectDTO project : projects) {
                    @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjectId(project.getId());
                    taskRepository.removeAll(projectTasks);
                }
                projectRepository.removeAll(projects);
            }
            repository.removeAll(collection);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<UserDTO> addAll(@Nullable final Collection<UserDTO> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<UserDTO> set(@Nullable final Collection<UserDTO> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<UserDTO> users = findAll();
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : users) {
                @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(user.getId());
                @NotNull final List<TaskDTO> tasks = taskRepository.findAll(user.getId());
                @NotNull final List<ProjectDTO> projects = projectRepository.findAll(user.getId());
                taskRepository.removeAll(tasks);
                sessionRepository.removeAll(sessions);
                for (@NotNull final ProjectDTO project : projects) {
                    @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjectId(project.getId());
                    taskRepository.removeAll(projectTasks);
                }
                projectRepository.removeAll(projects);
            }
            repository.set(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    public UserDTO add(@Nullable final UserDTO model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull List<UserDTO> users;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            users = repository.findAll();
        }
        finally {
            entityManager.close();
        }
        return users;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            user = repository.findOneById(id);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            user = repository.findOneByIndex(index);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO removeOne(@Nullable final UserDTO model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(model.getId());
            @NotNull final List<ProjectDTO> projects = projectRepository.findAll(model.getId());
            @NotNull final List<TaskDTO> tasks = taskRepository.findAll(model.getId());
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            sessionRepository.removeAll(sessions);
            for (@NotNull final ProjectDTO project : projects) {
                @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjectId(project.getId());
                taskRepository.removeAll(projectTasks);
            }
            projectRepository.removeAll(projects);
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO model = findOneById(id);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAll(model.getId());
            @NotNull final List<ProjectDTO> projects = projectRepository.findAll(model.getId());
            @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(model.getId());
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            sessionRepository.removeAll(sessions);
            for (@NotNull final ProjectDTO project : projects) {
                @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjectId(project.getId());
                taskRepository.removeAll(projectTasks);
            }
            projectRepository.removeAll(projects);
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final UserDTO model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAll(model.getId());
            @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(model.getId());
            @NotNull final List<ProjectDTO> projects = projectRepository.findAll(model.getId());
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            sessionRepository.removeAll(sessions);
            for (@NotNull final ProjectDTO project : projects) {
                @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjectId(project.getId());
                taskRepository.removeAll(projectTasks);
            }
            projectRepository.removeAll(projects);
            repository.removeOneByIndex(index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            @NotNull final ISessionDTORepository sessionRepository = new SessionDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
            @NotNull final List<UserDTO> users = findAll();
            entityManager.getTransaction().begin();
            for (@NotNull final UserDTO user : users) {
                @NotNull final List<TaskDTO> tasks = taskRepository.findAll(user.getId());
                @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(user.getId());
                @NotNull final List<ProjectDTO> projects = projectRepository.findAll(user.getId());
                taskRepository.removeAll(tasks);
                sessionRepository.removeAll(sessions);
                for (@NotNull final ProjectDTO project : projects) {
                    @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjectId(project.getId());
                    taskRepository.removeAll(projectTasks);
                }
                projectRepository.removeAll(projects);
            }
            repository.removeAll();
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        long size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            size = repository.getSize();
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsUser;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            existsUser = repository.existsById(id);
        }
        finally {
            entityManager.close();
        }
        return existsUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            user = repository.findByLogin(login);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDTO user;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            user = repository.findByEmail(email);
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        boolean existsUser;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            existsUser = repository.isLoginExist(login);
        }
        finally {
            entityManager.close();
        }
        return existsUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        boolean existsUser;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            existsUser = repository.isEmailExist(email);
        }
        finally {
            entityManager.close();
        }
        return existsUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = new UserDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return user;
    }

}
