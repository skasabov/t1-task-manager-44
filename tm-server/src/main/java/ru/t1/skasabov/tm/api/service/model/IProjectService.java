package ru.t1.skasabov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;

import java.util.Date;
import java.util.List;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    List<Project> findAll(@Nullable Sort sortType);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sortType);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name, @Nullable String description,
            @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
