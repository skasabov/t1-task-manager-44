package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.component.ISaltProvider;
import ru.t1.skasabov.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends ISaltProvider, IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDatabaseUserName();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

}
