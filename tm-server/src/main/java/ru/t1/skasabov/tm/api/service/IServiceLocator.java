package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.service.dto.*;
import ru.t1.skasabov.tm.api.service.model.IProjectService;
import ru.t1.skasabov.tm.api.service.model.IUserService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IProjectDTOService getProjectDTOService();

    @NotNull
    ITaskDTOService getTaskDTOService();

    @NotNull
    IProjectTaskDTOService getProjectTaskDTOService();

    @NotNull
    IUserDTOService getUserDTOService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDTOService getSessionDTOService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IConnectionService getConnectionService();

}
