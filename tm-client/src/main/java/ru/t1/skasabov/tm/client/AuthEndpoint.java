package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.skasabov.tm.dto.request.UserLoginRequest;
import ru.t1.skasabov.tm.dto.request.UserLogoutRequest;
import ru.t1.skasabov.tm.dto.request.UserProfileRequest;
import ru.t1.skasabov.tm.dto.response.UserLoginResponse;
import ru.t1.skasabov.tm.dto.response.UserLogoutResponse;
import ru.t1.skasabov.tm.dto.response.UserProfileResponse;

@NoArgsConstructor
public final class AuthEndpoint implements IAuthEndpoint {

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return IAuthEndpoint.newInstance().login(request);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return IAuthEndpoint.newInstance().logout(request);
    }

    @NotNull
    @Override
    public UserProfileResponse profile(@NotNull final UserProfileRequest request) {
        return IAuthEndpoint.newInstance().profile(request);
    }

}
